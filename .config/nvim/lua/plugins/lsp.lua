local M = {
	"williamboman/mason-lspconfig.nvim",
	event = "BufReadPost",
	dependencies = {
		"neovim/nvim-lspconfig",
		"williamboman/mason.nvim",
	},
}

function M.config()
	local capabilities = vim.lsp.protocol.make_client_capabilities()

	require("mason").setup()
	require("mason-lspconfig").setup({
		handlers = {
			function(server_name)
				require("lspconfig")[server_name].setup({
					capabilities = capabilities,
				})
			end,
		},
	})
end

return M
