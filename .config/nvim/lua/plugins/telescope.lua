--Fuzzy finder(Telescope)
return {
	"nvim-telescope/telescope.nvim",
	keys = { "<leader>F", "<leader>bb", "<leader>ff", "<leader>hh", "<leader>r", "<leader>bb" },
	dependencies = {
		"nvim-lua/plenary.nvim",
	},
	config = function()
		require("telescope").setup()
		local builtin = require("telescope.builtin")
		vim.keymap.set("n", "<leader>F", builtin.fd, {})
		vim.keymap.set("n", "<leader>f", builtin.current_buffer_fuzzy_find, {})
		vim.keymap.set("n", "<leader>hh", builtin.help_tags, {})
		vim.keymap.set("n", "<leader>bb", builtin.buffers, {})
	end,
}
