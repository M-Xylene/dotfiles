-- More highlighting with treesitter
local M = {
	"nvim-treesitter/nvim-treesitter",
	event = "BufRead",
}

function M.config()
	require("nvim-treesitter.configs").setup({
		highlight = { enable = true },
		indent = { enable = true },
	})
end

return M
