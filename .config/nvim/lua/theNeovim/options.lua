vim.g.mapleader = " "

vim.wo.number = true
vim.wo.relativenumber = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.shadafile = "NONE"

vim.bo.autoindent = true
vim.bo.smartindent = true
vim.opt.ignorecase = true
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.o.expandtab = true
vim.opt.shiftwidth = 2

vim.opt.swapfile = false
vim.opt.undodir = os.getenv("HOME") .. "/.vi/undodir"
vim.opt.undofile = true
vim.opt.path:append({ "**" })
vim.opt.clipboard:append({ "unnamedplus" })
vim.opt.completeopt = "menu,menuone,noselect"

vim.opt.guicursor = ""
vim.opt.termguicolors = true
vim.wo.scrolloff = 8
vim.wo.wrap = false
