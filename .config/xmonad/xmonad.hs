import XMonad
import Data.Monoid
import System.Exit

import XMonad.Layout.Fullscreen ( fullscreenSupportBorder )
import XMonad.Hooks.ManageDocks ( avoidStruts, docks )
import XMonad.Layout.Spacing ( spacingWithEdge )
import XMonad.Hooks.EwmhDesktops ( ewmh )
import XMonad.Util.SpawnOnce 

import XMonad.Util.EZConfig ( additionalKeysP, removeKeysP )

import qualified XMonad.StackSet as W
import qualified Data.Map as M

keybindings = [
  -- Terminal
  ("M-<Return>", spawn "kitty -e tmux -u -2")
  ,("M-S-<Return>", spawn "kitty")

 -- File Manager
 ,("M-e", spawn "kitty -o font_size=13 -e yazi")

  -- Application launcher
  ,("C-<Space>", spawn "rofi -show drun -icon-theme 'Papirus'")

 -- Brightness
 ,("C-S-<Right>", spawn "brillo -A 5 -q ")
 ,("C-S-<Left>",  spawn "brillo -U 5 -q ")

 -- Volume
 ,("<XF86AudioRaiseVolume>",   spawn "pamixer -u -i 10 --allow-boost ")
 ,("<XF86AudioLowerVolume>", spawn "pamixer -u -d 10 --allow-boost ")
 ,("<XF86AudioMute>",      spawn "pamixer -t")

 -- Screenshot
 ,("<Print>",   spawn "scrot '%Y-%m-%d_%H-%M-%S.png' -e 'mv $f ~/Pictures/Screenshots/' -q 100")
 ,("C-<Print>", spawn "import png:- | xclip -selection clipboard -t image/png")

 -- Notification
 , ("C-`",   spawn "dunstctl history-pop")
 , ("C-S-`", spawn "dunstctl close")

 -- Dmenu Scripts
 ,("M-S-l", spawn"dm-logout")

 -- Swapping Master Window
 ,("C-S-<Return>", windows W.swapMaster)

 -- Switch Focus
 ,("M-l", windows W.focusUp)

  -- Shrink the master area
 ,("M-S-h", sendMessage Shrink)

  -- Expand the master area
 ,("M-S-t", sendMessage Expand)

 -- Killing window
 ,("M-c", kill)
    
  -- Full screen
 ,("M-S-<Space>", sendMessage NextLayout)

  -- Toggle the polybar bar
 ,("M-S-b", spawn "polybar-msg cmd toggle ")

 ]

removedKeys = [
  ("M-<Space>")
 ,("M-h")
 ,("M-t")
 ,("C-p")
 ,("M-p")
 ]

layouts = tiled ||| Full
  where
     tiled   = spacingWithEdge 3 $ avoidStruts $ Tall nmaster delta ratio
     nmaster = 1
     ratio   = 1/2
     delta   = 3/100

windowRules :: ManageHook
windowRules = composeAll
   [ resource  =? "desktop_window" --> doIgnore
    ,resource  =? "kdesktop"       --> doIgnore ] {- <+> namedScratchpadManageHook myScratchPads -}

startupServices :: X ()
startupServices = do
  spawn "$HOME/.config/polybar/XMonadPolybar.sh"
  spawnOnce "nm-applet &"
  spawnOnce "~/.fehbg &"
  spawnOnce "picom --vsync &"

main :: IO ()
main = do
    xmonad $ docks $ ewmh $ fullscreenSupportBorder $ def {
        focusFollowsMouse  = False,
        clickJustFocuses   = False,
        borderWidth        = 1,
        modMask            = mod1Mask,
        workspaces         = ["1", "2", "3", "4"],
        normalBorderColor  = "#1B1D1E",
        focusedBorderColor = "#fe8019",
        layoutHook         = layouts,
        manageHook         = windowRules,
        startupHook        = startupServices
    } `additionalKeysP` keybindings `removeKeysP` removedKeys
