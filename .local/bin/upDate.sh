DATEFILE=~/.lastupdate.txt
if [ -f $DATEFILE ]; then
    echo $(date '+%a %b %d - %I:%M %p') > $DATEFILE
elif [ ! -f $DATEFILE ]; then
    echo $(date '+%a %b %d - %I:%M %p') > $DATEFILE
fi
